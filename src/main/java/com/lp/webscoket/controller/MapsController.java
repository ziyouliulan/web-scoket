package com.lp.webscoket.controller;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.lp.webscoket.config.WebSocketServer;
import com.lp.webscoket.domain.DmsAddress;
import com.lp.webscoket.domain.DmsBbDealers;
import com.lp.webscoket.entity.Address;
import com.lp.webscoket.entity.Saleinfo;
import com.lp.webscoket.service.DmsAddressService;
import com.lp.webscoket.service.DmsBbDealersService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

//@Controller
@Controller
@RequestMapping("/map")
@Slf4j
public class MapsController {

    @Autowired
    DmsBbDealersService dmsBbDealersService;
    @Autowired
    DmsAddressService dmsAddressService;

    @Autowired
    WebSocketServer webSocketServer;
    /**
     * 跳转到websocketDemo.html页面，携带自定义的cid信息。
     * http://localhost:8081/demo/toWebSocketDemo/user-1
     *
     * @param cid
     * @param model
     * @return
     */
    @RequestMapping("/toWebSocketDemo/{cid}")
    public String toWebSocketDemo(@PathVariable String cid, Model model) {
        model.addAttribute("cid", cid);
        return "websocketDemo";
    }

    @RequestMapping(value = "/saleinfo")
    public ResponseEntity saleinfo( Saleinfo saleinfo, @RequestParam String callback) {
        log.info("saleinfo::{}",saleinfo);
        RestTemplate restTemplate = new RestTemplate();

        List<DmsBbDealers> list = dmsBbDealersService.selectleft(saleinfo.getDealerId(),saleinfo.getCompanyId());

        //经纬度已存在
        if (list.size() > 0 && StringUtils.isNotEmpty(list.get(0).getLon()) && StringUtils.isNotEmpty(list.get(0).getLat())){

            System.out.println("list = " + list.get(0));

            webSocketServer.sendToOne("user-1", JSONObject.toJSONString(list.get(0)));
            return ResponseEntity.ok(callback + "(请求成功)");
            //通知前端
        }else if (list.size() > 0){
//            DmsBbDealers dmsBbDealers = list.get(0);

            String  url = "https://restapi.amap.com/v3/geocode/geo?key=2e6d9af820323c233ab5e1fe5339c7c9&address="+saleinfo.getAddress()+"&city=";
            ResponseEntity<String> forEntity = restTemplate.getForEntity(url, String.class);
            Address address = JSON.parseObject(forEntity.getBody(), Address.class);
            if ("10000".equals(address.getInfocode())){
                DmsAddress dmsAddress = new DmsAddress();

                String[] split = address.getGeocodes().get(0).getLocation().split(",");
                System.out.println("split = " + split);
                dmsAddress.setLon(split[0]);
                dmsAddress.setLat(split[1]);
                dmsAddress.setCompanyid(saleinfo.getCompanyId());
                dmsAddress.setDealerid(saleinfo.getDealerId());

                dmsAddressService.save(dmsAddress);

            }else {
                return ResponseEntity.badRequest().body(callback + "(第三方接口异常)");
                //返回错误信息
            }
            return ResponseEntity.ok().body(callback + "(经纬度更新成功)");
        }else {
            //参数异常
            return ResponseEntity.badRequest().body(callback + "(参数异常)");
        }



//        1.去历史经纬度
//            2.没有的情况下
//                a. java代码发起解析请求 解析完成 告知前端绘制锚点 告知帆软结果
////                b. scoket 通知js发起解析请求，解析完成，socket 告知后台解析结果。

//        String jsonStr = "{'name':'" + 11111 + "'}";
//        String jsonString = JSON.toJSONString(saleinfo);
//        return ResponseEntity.ok(callback + "(" + jsonString + ")");
    }
    @RequestMapping(value = "/test")
    public ResponseEntity test( ) {

        return ResponseEntity.ok(dmsBbDealersService.list());
    }
}
