package com.lp.webscoket.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lp.webscoket.domain.DmsBbDealers;
import com.lp.webscoket.service.DmsBbDealersService;
import com.lp.webscoket.mapper.DmsBbDealersMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
* @author jm
* @description 针对表【DMS_BB_DEALERS(经销商基本信息表)】的数据库操作Service实现
* @createDate 2023-11-13 10:12:13
*/
@Service
public class DmsBbDealersServiceImpl extends ServiceImpl<DmsBbDealersMapper, DmsBbDealers>
    implements DmsBbDealersService{

    @Autowired
    DmsBbDealersMapper dmsBbDealersMapper;
   public List<DmsBbDealers> selectleft(Long dealerId, Long companyId){
        return dmsBbDealersMapper.selectleft(dealerId,companyId);
    }
}




