package com.lp.webscoket.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lp.webscoket.domain.DmsAddress;
import com.lp.webscoket.service.DmsAddressService;
import com.lp.webscoket.mapper.DmsAddressMapper;
import org.springframework.stereotype.Service;

/**
* @author jm
* @description 针对表【DMS_ADDRESS】的数据库操作Service实现
* @createDate 2023-11-13 10:49:56
*/
@Service
public class DmsAddressServiceImpl extends ServiceImpl<DmsAddressMapper, DmsAddress>
    implements DmsAddressService{

}




