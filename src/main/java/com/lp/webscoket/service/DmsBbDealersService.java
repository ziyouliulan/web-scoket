package com.lp.webscoket.service;

import com.lp.webscoket.domain.DmsBbDealers;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
* @author jm
* @description 针对表【DMS_BB_DEALERS(经销商基本信息表)】的数据库操作Service
* @createDate 2023-11-13 10:12:13
*/
public interface DmsBbDealersService extends IService<DmsBbDealers> {
   public List<DmsBbDealers> selectleft(@Param("dealerId") Long dealerId,@Param("companyId") Long companyId);
}
