package com.lp.webscoket.service;

import com.lp.webscoket.domain.DmsAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author jm
* @description 针对表【DMS_ADDRESS】的数据库操作Service
* @createDate 2023-11-13 10:49:56
*/
public interface DmsAddressService extends IService<DmsAddress> {

}
