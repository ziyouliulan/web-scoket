package com.lp.webscoket.mapper;

import com.lp.webscoket.domain.DmsBbDealers;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.HashMap;
import java.util.List;

/**
* @author jm
* @description 针对表【DMS_BB_DEALERS(经销商基本信息表)】的数据库操作Mapper
* @createDate 2023-11-13 10:12:13
* @Entity com.lp.webscoket.domain.DmsBbDealers
*/
@Mapper
public interface DmsBbDealersMapper extends BaseMapper<DmsBbDealers> {

      List<DmsBbDealers> selectleft( @Param("dealerId") Long dealerId,@Param("companyId") Long companyId);

      @Select("SELECT d.ADDRESS,d.DEALER_ID,d.COMPANY_ID , d.DEALER_NAME, da.LON, da.LAT from dms_bb_dealers d " +
              "LEFT JOIN Dms_address da on da.COMPANYID = d.COMPANY_ID and da.DEALERID = d.DEALER_ID WHERE d.COMPANY_ID > ?1 and   ROWNUM <10 ")
      List<DmsBbDealers> selectleft2( Long companyId );

}




