package com.lp.webscoket.mapper;

import com.lp.webscoket.domain.DmsAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author jm
* @description 针对表【DMS_ADDRESS】的数据库操作Mapper
* @createDate 2023-11-13 10:49:56
* @Entity com.lp.webscoket.domain.DmsAddress
*/
public interface DmsAddressMapper extends BaseMapper<DmsAddress> {

}




