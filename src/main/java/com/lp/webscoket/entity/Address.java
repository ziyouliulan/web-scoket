package com.lp.webscoket.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class Address {


    private String status;
    private String info;
    private String infocode;
    private String count;
    private List<GeocodesDTO> geocodes;

    @NoArgsConstructor
    @Data
    public static class GeocodesDTO {
        private String formattedAddress;
        private String country;
        private String province;
        private String citycode;
        private String city;
        private String district;
        private List<?> township;
        private NeighborhoodDTO neighborhood;
        private BuildingDTO building;
        private String adcode;
        private String street;
        private String number;
        private String location;
        private String level;

        @NoArgsConstructor
        @Data
        public static class NeighborhoodDTO {
            private List<?> name;
            private List<?> type;
        }

        @NoArgsConstructor
        @Data
        public static class BuildingDTO {
            private List<?> name;
            private List<?> type;
        }
    }
}
