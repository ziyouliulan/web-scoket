package com.lp.webscoket.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Saleinfo implements Serializable {

    String address;

    Long dealerId;

    Long companyId;
    //0 销售公司，1:服务站 2经销站
    String type;

}
