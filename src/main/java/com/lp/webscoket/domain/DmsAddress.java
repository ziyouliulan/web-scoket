package com.lp.webscoket.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName DMS_ADDRESS
 */
@TableName(value ="DMS_ADDRESS")
@Data
public class DmsAddress implements Serializable {
    /**
     * 
     */
    @TableId
    private Long dealerid;

    /**
     * 
     */
//    @TableId
    private Long companyid;

    /**
     * 维度
     */
    private String lat;

    /**
     * 经度
     */
    private String lon;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DmsAddress other = (DmsAddress) that;
        return (this.getDealerid() == null ? other.getDealerid() == null : this.getDealerid().equals(other.getDealerid()))
            && (this.getCompanyid() == null ? other.getCompanyid() == null : this.getCompanyid().equals(other.getCompanyid()))
            && (this.getLat() == null ? other.getLat() == null : this.getLat().equals(other.getLat()))
            && (this.getLon() == null ? other.getLon() == null : this.getLon().equals(other.getLon()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getDealerid() == null) ? 0 : getDealerid().hashCode());
        result = prime * result + ((getCompanyid() == null) ? 0 : getCompanyid().hashCode());
        result = prime * result + ((getLat() == null) ? 0 : getLat().hashCode());
        result = prime * result + ((getLon() == null) ? 0 : getLon().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", dealerid=").append(dealerid);
        sb.append(", companyid=").append(companyid);
        sb.append(", lat=").append(lat);
        sb.append(", lon=").append(lon);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}