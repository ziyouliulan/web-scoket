package com.lp.webscoket.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 经销商基本信息表
 * @TableName DMS_BB_DEALERS
 */
@TableName(value ="DMS_BB_DEALERS")
@Data
public class DmsBbDealers implements Serializable {
    /**
     * 公司ID号。实体的唯一标识，用户识别数据主体，在本模型中，4S站、2S站、网点、分工厂、手拉手服务站统称为经销商，经销商代码是有上端统一确定的，下端无法更改，公司ID号为0的数据表示是共享数据
     */
    @TableId
    private Long companyId;

    /**
     * 服务站(经销商)ID号
     */
//    @TableId
    private Long dealerId;

    /**
     * 表示属于哪个经销商公司实体
     */
    private Long dealerCompanyId;

    /**
     * 服务站或经销商的业务代码，特定情况下可以修改，只作为业务标识，不作为数据标识
     */
    private String dealerCode;

    /**
     * 服务站(经销商)名称
     */
    private String dealerName;

    /**
     * 服务站(经销商)简称
     */
    private String dealerShortName;

    /**
     * 服务站(经销商)ID号(K3)
     */
    private String dealerIdK3;

    /**
     * 经销商级别
     */
    private Long dealerLevel;

    /**
     * 授权商圈1107
     */
    private Long authorizedBcd;

    /**
     * 加盟日期
     */
    private String joinTime;

    /**
     * 营业状态1108
     */
    private Long bizStatus;

    /**
     * 国家，来源：区域定义表
     */
    private Long country;

    /**
     * 省份，来源：区域定义表
     */
    private Long province;

    /**
     * 城市，来源：区域定义表
     */
    private Long city;

    /**
     * 来源：区域定义表
     */
    private Long county;

    /**
     * 验收日期
     */
    private String checkedToDate;

    /**
     * 法人代表
     */
    private String incorporator;

    /**
     * 注册资金
     */
    private Long registeredCapital;

    /**
     * 保证金额度
     */
    private Long margin;

    /**
     * 传真号码
     */
    private String faxNo;

    /**
     * 法人电话
     */
    private String incorporatorTel;

    /**
     * 法人手机
     */
    private String incorporatorMob;

    /**
     * 热线电话
     */
    private String hotLine;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 销售大区ID号
     */
    private Long dlrRegionId;

    /**
     * 大区名称
     */
    private String dlrRegionName;

    /**
     * 销售区域ID号
     */
    private Long dlrSectorId;

    /**
     * 区域名称
     */
    private String dlrSectorName;

    /**
     * 退网日期
     */
    private String revokeDate;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 公司主页
     */
    private String website;

    /**
     * 邮政编码
     */
    private String zipCode;

    /**
     * 备注
     */
    private String remark;

    /**
     * 经销商锁定
     */
    private Long dealerLock;

    /**
     * 人员锁定
     */
    private Long empLock;

    /**
     * 系统保留字段，表示当前数据版本，用于控制业务并发，无其它业务含义
     */
    private Long version;

    /**
     * 系统保留字段，记录数据创建用户，用于审计，无业务含义，如果是单据，可以作为制单人使用
     */
    private Long createdBy;

    /**
     * 系统保留字段，记录数据创建时间，用于系统审计，无实际业务含义
     */
    private String createdTime;

    /**
     * 系统保留字段，记录数据最后更新用户，用于审计，无业务含义
     */
    private Long updatedBy;

    /**
     * 系统保留字段，记录数据最后时间，用于审计，无业务含义
     */
    private String updatedTime;

    /**
     * 享受政策截止时间
     */
    private String enjoyPolicyEndDate;

    /**
     * 享受政策最大限额台数
     */
    private Long enjoyPolicyQuota;

    /**
     * 已享受政策台数
     */
    private Long enjoyPolicyAlready;

    /**
     * 开户银行
     */
    private String bankName;

    /**
     * 开户账号
     */
    private String bankAccount;

    /**
     * 税号
     */
    private String taxNo;

    /**
     * 组织机构代码
     */
    private String registeredNo;

    /**
     * 联系人电话
     */
    private String linkmanPhone;

    /**
     * 联系人
     */
    private String linkmanName;

    /**
     * 政策单台金额
     */
    private Long enjoyPolicyPrice;

    /**
     * 营业时间
     */
    private String bizHours;

    /**
     * 详细地址
     */
    private String registeredAddress;

    /**
     * 表示属于哪个经销商公司实体
     */
    private Long storeType;

    /**
     * 是否可预约
     */
    private Long canOrder;

    /**
     * 坐标
     */
    private String coordinate;
    private String lon;
    private String lat;


    /**
     * 有无开票资质
     */
    private Long isBillingQualification;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        DmsBbDealers other = (DmsBbDealers) that;
        return (this.getCompanyId() == null ? other.getCompanyId() == null : this.getCompanyId().equals(other.getCompanyId()))
            && (this.getDealerId() == null ? other.getDealerId() == null : this.getDealerId().equals(other.getDealerId()))
            && (this.getDealerCompanyId() == null ? other.getDealerCompanyId() == null : this.getDealerCompanyId().equals(other.getDealerCompanyId()))
            && (this.getDealerCode() == null ? other.getDealerCode() == null : this.getDealerCode().equals(other.getDealerCode()))
            && (this.getDealerName() == null ? other.getDealerName() == null : this.getDealerName().equals(other.getDealerName()))
            && (this.getDealerShortName() == null ? other.getDealerShortName() == null : this.getDealerShortName().equals(other.getDealerShortName()))
            && (this.getDealerIdK3() == null ? other.getDealerIdK3() == null : this.getDealerIdK3().equals(other.getDealerIdK3()))
            && (this.getDealerLevel() == null ? other.getDealerLevel() == null : this.getDealerLevel().equals(other.getDealerLevel()))
            && (this.getAuthorizedBcd() == null ? other.getAuthorizedBcd() == null : this.getAuthorizedBcd().equals(other.getAuthorizedBcd()))
            && (this.getJoinTime() == null ? other.getJoinTime() == null : this.getJoinTime().equals(other.getJoinTime()))
            && (this.getBizStatus() == null ? other.getBizStatus() == null : this.getBizStatus().equals(other.getBizStatus()))
            && (this.getCountry() == null ? other.getCountry() == null : this.getCountry().equals(other.getCountry()))
            && (this.getProvince() == null ? other.getProvince() == null : this.getProvince().equals(other.getProvince()))
            && (this.getCity() == null ? other.getCity() == null : this.getCity().equals(other.getCity()))
            && (this.getCounty() == null ? other.getCounty() == null : this.getCounty().equals(other.getCounty()))
            && (this.getCheckedToDate() == null ? other.getCheckedToDate() == null : this.getCheckedToDate().equals(other.getCheckedToDate()))
            && (this.getIncorporator() == null ? other.getIncorporator() == null : this.getIncorporator().equals(other.getIncorporator()))
            && (this.getRegisteredCapital() == null ? other.getRegisteredCapital() == null : this.getRegisteredCapital().equals(other.getRegisteredCapital()))
            && (this.getMargin() == null ? other.getMargin() == null : this.getMargin().equals(other.getMargin()))
            && (this.getFaxNo() == null ? other.getFaxNo() == null : this.getFaxNo().equals(other.getFaxNo()))
            && (this.getIncorporatorTel() == null ? other.getIncorporatorTel() == null : this.getIncorporatorTel().equals(other.getIncorporatorTel()))
            && (this.getIncorporatorMob() == null ? other.getIncorporatorMob() == null : this.getIncorporatorMob().equals(other.getIncorporatorMob()))
            && (this.getHotLine() == null ? other.getHotLine() == null : this.getHotLine().equals(other.getHotLine()))
            && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
            && (this.getDlrRegionId() == null ? other.getDlrRegionId() == null : this.getDlrRegionId().equals(other.getDlrRegionId()))
            && (this.getDlrRegionName() == null ? other.getDlrRegionName() == null : this.getDlrRegionName().equals(other.getDlrRegionName()))
            && (this.getDlrSectorId() == null ? other.getDlrSectorId() == null : this.getDlrSectorId().equals(other.getDlrSectorId()))
            && (this.getDlrSectorName() == null ? other.getDlrSectorName() == null : this.getDlrSectorName().equals(other.getDlrSectorName()))
            && (this.getRevokeDate() == null ? other.getRevokeDate() == null : this.getRevokeDate().equals(other.getRevokeDate()))
            && (this.getAddress() == null ? other.getAddress() == null : this.getAddress().equals(other.getAddress()))
            && (this.getWebsite() == null ? other.getWebsite() == null : this.getWebsite().equals(other.getWebsite()))
            && (this.getZipCode() == null ? other.getZipCode() == null : this.getZipCode().equals(other.getZipCode()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getDealerLock() == null ? other.getDealerLock() == null : this.getDealerLock().equals(other.getDealerLock()))
            && (this.getEmpLock() == null ? other.getEmpLock() == null : this.getEmpLock().equals(other.getEmpLock()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getCreatedBy() == null ? other.getCreatedBy() == null : this.getCreatedBy().equals(other.getCreatedBy()))
            && (this.getCreatedTime() == null ? other.getCreatedTime() == null : this.getCreatedTime().equals(other.getCreatedTime()))
            && (this.getUpdatedBy() == null ? other.getUpdatedBy() == null : this.getUpdatedBy().equals(other.getUpdatedBy()))
            && (this.getUpdatedTime() == null ? other.getUpdatedTime() == null : this.getUpdatedTime().equals(other.getUpdatedTime()))
            && (this.getEnjoyPolicyEndDate() == null ? other.getEnjoyPolicyEndDate() == null : this.getEnjoyPolicyEndDate().equals(other.getEnjoyPolicyEndDate()))
            && (this.getEnjoyPolicyQuota() == null ? other.getEnjoyPolicyQuota() == null : this.getEnjoyPolicyQuota().equals(other.getEnjoyPolicyQuota()))
            && (this.getEnjoyPolicyAlready() == null ? other.getEnjoyPolicyAlready() == null : this.getEnjoyPolicyAlready().equals(other.getEnjoyPolicyAlready()))
            && (this.getBankName() == null ? other.getBankName() == null : this.getBankName().equals(other.getBankName()))
            && (this.getBankAccount() == null ? other.getBankAccount() == null : this.getBankAccount().equals(other.getBankAccount()))
            && (this.getTaxNo() == null ? other.getTaxNo() == null : this.getTaxNo().equals(other.getTaxNo()))
            && (this.getRegisteredNo() == null ? other.getRegisteredNo() == null : this.getRegisteredNo().equals(other.getRegisteredNo()))
            && (this.getLinkmanPhone() == null ? other.getLinkmanPhone() == null : this.getLinkmanPhone().equals(other.getLinkmanPhone()))
            && (this.getLinkmanName() == null ? other.getLinkmanName() == null : this.getLinkmanName().equals(other.getLinkmanName()))
            && (this.getEnjoyPolicyPrice() == null ? other.getEnjoyPolicyPrice() == null : this.getEnjoyPolicyPrice().equals(other.getEnjoyPolicyPrice()))
            && (this.getBizHours() == null ? other.getBizHours() == null : this.getBizHours().equals(other.getBizHours()))
            && (this.getRegisteredAddress() == null ? other.getRegisteredAddress() == null : this.getRegisteredAddress().equals(other.getRegisteredAddress()))
            && (this.getStoreType() == null ? other.getStoreType() == null : this.getStoreType().equals(other.getStoreType()))
            && (this.getCanOrder() == null ? other.getCanOrder() == null : this.getCanOrder().equals(other.getCanOrder()))
            && (this.getCoordinate() == null ? other.getCoordinate() == null : this.getCoordinate().equals(other.getCoordinate()))
            && (this.getIsBillingQualification() == null ? other.getIsBillingQualification() == null : this.getIsBillingQualification().equals(other.getIsBillingQualification()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getCompanyId() == null) ? 0 : getCompanyId().hashCode());
        result = prime * result + ((getDealerId() == null) ? 0 : getDealerId().hashCode());
        result = prime * result + ((getDealerCompanyId() == null) ? 0 : getDealerCompanyId().hashCode());
        result = prime * result + ((getDealerCode() == null) ? 0 : getDealerCode().hashCode());
        result = prime * result + ((getDealerName() == null) ? 0 : getDealerName().hashCode());
        result = prime * result + ((getDealerShortName() == null) ? 0 : getDealerShortName().hashCode());
        result = prime * result + ((getDealerIdK3() == null) ? 0 : getDealerIdK3().hashCode());
        result = prime * result + ((getDealerLevel() == null) ? 0 : getDealerLevel().hashCode());
        result = prime * result + ((getAuthorizedBcd() == null) ? 0 : getAuthorizedBcd().hashCode());
        result = prime * result + ((getJoinTime() == null) ? 0 : getJoinTime().hashCode());
        result = prime * result + ((getBizStatus() == null) ? 0 : getBizStatus().hashCode());
        result = prime * result + ((getCountry() == null) ? 0 : getCountry().hashCode());
        result = prime * result + ((getProvince() == null) ? 0 : getProvince().hashCode());
        result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
        result = prime * result + ((getCounty() == null) ? 0 : getCounty().hashCode());
        result = prime * result + ((getCheckedToDate() == null) ? 0 : getCheckedToDate().hashCode());
        result = prime * result + ((getIncorporator() == null) ? 0 : getIncorporator().hashCode());
        result = prime * result + ((getRegisteredCapital() == null) ? 0 : getRegisteredCapital().hashCode());
        result = prime * result + ((getMargin() == null) ? 0 : getMargin().hashCode());
        result = prime * result + ((getFaxNo() == null) ? 0 : getFaxNo().hashCode());
        result = prime * result + ((getIncorporatorTel() == null) ? 0 : getIncorporatorTel().hashCode());
        result = prime * result + ((getIncorporatorMob() == null) ? 0 : getIncorporatorMob().hashCode());
        result = prime * result + ((getHotLine() == null) ? 0 : getHotLine().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getDlrRegionId() == null) ? 0 : getDlrRegionId().hashCode());
        result = prime * result + ((getDlrRegionName() == null) ? 0 : getDlrRegionName().hashCode());
        result = prime * result + ((getDlrSectorId() == null) ? 0 : getDlrSectorId().hashCode());
        result = prime * result + ((getDlrSectorName() == null) ? 0 : getDlrSectorName().hashCode());
        result = prime * result + ((getRevokeDate() == null) ? 0 : getRevokeDate().hashCode());
        result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
        result = prime * result + ((getWebsite() == null) ? 0 : getWebsite().hashCode());
        result = prime * result + ((getZipCode() == null) ? 0 : getZipCode().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getDealerLock() == null) ? 0 : getDealerLock().hashCode());
        result = prime * result + ((getEmpLock() == null) ? 0 : getEmpLock().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
        result = prime * result + ((getCreatedTime() == null) ? 0 : getCreatedTime().hashCode());
        result = prime * result + ((getUpdatedBy() == null) ? 0 : getUpdatedBy().hashCode());
        result = prime * result + ((getUpdatedTime() == null) ? 0 : getUpdatedTime().hashCode());
        result = prime * result + ((getEnjoyPolicyEndDate() == null) ? 0 : getEnjoyPolicyEndDate().hashCode());
        result = prime * result + ((getEnjoyPolicyQuota() == null) ? 0 : getEnjoyPolicyQuota().hashCode());
        result = prime * result + ((getEnjoyPolicyAlready() == null) ? 0 : getEnjoyPolicyAlready().hashCode());
        result = prime * result + ((getBankName() == null) ? 0 : getBankName().hashCode());
        result = prime * result + ((getBankAccount() == null) ? 0 : getBankAccount().hashCode());
        result = prime * result + ((getTaxNo() == null) ? 0 : getTaxNo().hashCode());
        result = prime * result + ((getRegisteredNo() == null) ? 0 : getRegisteredNo().hashCode());
        result = prime * result + ((getLinkmanPhone() == null) ? 0 : getLinkmanPhone().hashCode());
        result = prime * result + ((getLinkmanName() == null) ? 0 : getLinkmanName().hashCode());
        result = prime * result + ((getEnjoyPolicyPrice() == null) ? 0 : getEnjoyPolicyPrice().hashCode());
        result = prime * result + ((getBizHours() == null) ? 0 : getBizHours().hashCode());
        result = prime * result + ((getRegisteredAddress() == null) ? 0 : getRegisteredAddress().hashCode());
        result = prime * result + ((getStoreType() == null) ? 0 : getStoreType().hashCode());
        result = prime * result + ((getCanOrder() == null) ? 0 : getCanOrder().hashCode());
        result = prime * result + ((getCoordinate() == null) ? 0 : getCoordinate().hashCode());
        result = prime * result + ((getIsBillingQualification() == null) ? 0 : getIsBillingQualification().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", companyId=").append(companyId);
        sb.append(", dealerId=").append(dealerId);
        sb.append(", dealerCompanyId=").append(dealerCompanyId);
        sb.append(", dealerCode=").append(dealerCode);
        sb.append(", dealerName=").append(dealerName);
        sb.append(", dealerShortName=").append(dealerShortName);
        sb.append(", dealerIdK3=").append(dealerIdK3);
        sb.append(", dealerLevel=").append(dealerLevel);
        sb.append(", authorizedBcd=").append(authorizedBcd);
        sb.append(", joinTime=").append(joinTime);
        sb.append(", bizStatus=").append(bizStatus);
        sb.append(", country=").append(country);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", county=").append(county);
        sb.append(", checkedToDate=").append(checkedToDate);
        sb.append(", incorporator=").append(incorporator);
        sb.append(", registeredCapital=").append(registeredCapital);
        sb.append(", margin=").append(margin);
        sb.append(", faxNo=").append(faxNo);
        sb.append(", incorporatorTel=").append(incorporatorTel);
        sb.append(", incorporatorMob=").append(incorporatorMob);
        sb.append(", hotLine=").append(hotLine);
        sb.append(", email=").append(email);
        sb.append(", dlrRegionId=").append(dlrRegionId);
        sb.append(", dlrRegionName=").append(dlrRegionName);
        sb.append(", dlrSectorId=").append(dlrSectorId);
        sb.append(", dlrSectorName=").append(dlrSectorName);
        sb.append(", revokeDate=").append(revokeDate);
        sb.append(", address=").append(address);
        sb.append(", website=").append(website);
        sb.append(", zipCode=").append(zipCode);
        sb.append(", remark=").append(remark);
        sb.append(", dealerLock=").append(dealerLock);
        sb.append(", empLock=").append(empLock);
        sb.append(", version=").append(version);
        sb.append(", createdBy=").append(createdBy);
        sb.append(", createdTime=").append(createdTime);
        sb.append(", updatedBy=").append(updatedBy);
        sb.append(", updatedTime=").append(updatedTime);
        sb.append(", enjoyPolicyEndDate=").append(enjoyPolicyEndDate);
        sb.append(", enjoyPolicyQuota=").append(enjoyPolicyQuota);
        sb.append(", enjoyPolicyAlready=").append(enjoyPolicyAlready);
        sb.append(", bankName=").append(bankName);
        sb.append(", bankAccount=").append(bankAccount);
        sb.append(", taxNo=").append(taxNo);
        sb.append(", registeredNo=").append(registeredNo);
        sb.append(", linkmanPhone=").append(linkmanPhone);
        sb.append(", linkmanName=").append(linkmanName);
        sb.append(", enjoyPolicyPrice=").append(enjoyPolicyPrice);
        sb.append(", bizHours=").append(bizHours);
        sb.append(", registeredAddress=").append(registeredAddress);
        sb.append(", storeType=").append(storeType);
        sb.append(", canOrder=").append(canOrder);
        sb.append(", coordinate=").append(coordinate);
        sb.append(", isBillingQualification=").append(isBillingQualification);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}