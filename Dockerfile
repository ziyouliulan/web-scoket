# 拉取jdk8作为基础镜像
FROM openjdk:8
# 作者
LABEL author="zp wang <test@qq.com>" describe="test image"
# 添加jar到镜像并命名为text.jar
ADD  target/web-scoket-0.0.1-SNAPSHOT.jar /app/app.jar
# 镜像启动后暴露的端口
EXPOSE 8082
# jar运行命令，参数使用逗号隔开
ENTRYPOINT ["java","-jar","/app/app.jar"]